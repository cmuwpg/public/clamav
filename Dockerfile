FROM debian:buster-slim

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    clamav-freshclam clamav-daemon ca-certificates

RUN mv /etc/clamav/freshclam.conf /etc/clamav/freshclam.conf.dist
RUN mv /etc/clamav/clamd.conf /etc/clamav/clamd.conf.dist

COPY freshclam.conf /etc/clamav/freshclam.conf
COPY clamd.conf /etc/clamav/clamd.conf

RUN freshclam
